# Build stage
FROM messense/rust-musl-cross:x86_64-musl AS builder
WORKDIR /app
COPY . .
RUN cargo build -p adsbx_screenshot_service --release

FROM alpine:3.18.3 AS final
RUN apk add --no-cache chromium ca-certificates tzdata
ENV TZ=Europe/Paris
COPY .env /.env
COPY --from=builder /app/target/x86_64-unknown-linux-musl/release/adsbx_screenshot_service* /app
CMD ["/app/adsbx_screenshot_service"]
