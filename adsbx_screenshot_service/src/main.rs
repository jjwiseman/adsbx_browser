use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use adsbx_screenshot::{AdsbxBrowser, AdsbxBrowserOptions};
use serde::Deserialize;
use log::{info, error};

#[derive(Deserialize)]
struct ScreenshotParams {
    // Comma-separated list of ICAOs
    icaos: Option<String>,
    // Comma-separated list of registration numbers
    regs: Option<String>,
    // Image width, default is 800
    width: Option<u32>,
    // Image height, default is 600
    height: Option<u32>,
    // Show advertisements, default is false
    show_ads: Option<bool>,
    // Show information block, default is false
    show_infoblock: Option<bool>,
    // Zoom level, default is 10.0
    zoom: Option<f32>,
    // Disable isolation mode, default is false
    no_isolation: Option<bool>,
    // Custom URL for the ADS-B Exchange map
    url: Option<String>,
}

async fn screenshot(params: web::Query<ScreenshotParams>) -> impl Responder {
    let width = params.width.unwrap_or(800);
    let height = params.height.unwrap_or(600);

    let mut browser = match AdsbxBrowser::new((width, height)) {
        Ok(b) => b,
        Err(e) => {
            error!("Error initializing browser: {:?}", e);
            return HttpResponse::InternalServerError().finish();
        }
    };

    let mut options = if let Some(ref url) = params.url {
        match AdsbxBrowserOptions::try_from(url.as_str()) {
            Ok(mut opts) => {
                if let Some(show_infoblock) = params.show_infoblock {
                    opts.hide_infoblock = !show_infoblock;
                }
                opts
            }
            Err(e) => {
                error!("Error parsing URL: {:?}", e);
                return HttpResponse::BadRequest().body(format!("Error parsing URL: {:?}", e));
            }
        }
    } else {
        AdsbxBrowserOptions {
            icaos: params
                .icaos
                .as_deref()
                .unwrap_or("")
                .split(',')
                .filter(|s| !s.is_empty())
                .map(String::from)
                .collect(),
            regs: params
                .regs
                .as_deref()
                .unwrap_or("")
                .split(',')
                .filter(|s| !s.is_empty())
                .map(String::from)
                .collect(),
            delete_ads: !params.show_ads.unwrap_or(false),
            hide_infoblock: !params.show_infoblock.unwrap_or(false),
            zoom: params.zoom.unwrap_or(10.0),
            no_isolation: params.no_isolation.unwrap_or(false),
            ..Default::default()
        }
    };

    if let Some(zoom) = params.zoom {
        options.zoom = zoom;
    }

    info!("Screenshotting {}", options.to_url());

    let screenshot = match browser.screenshot(&options) {
        Ok(s) => s,
        Err(e) => {
            error!("Error taking screenshot: {:?}", e);
            return HttpResponse::InternalServerError().finish();
        }
    };

    HttpResponse::Ok()
        .content_type("image/png")
        .body(screenshot.data)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    info!("Starting server on port 8080");
    HttpServer::new(|| App::new().route("/screenshot", web::get().to(screenshot)))
        .bind("0.0.0.0:8080")?
        .run()
        .await
}
